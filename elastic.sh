#!/bin/bash
chmod 600 gitserver.pem
/var/lib/jenkins/.local/bin/aws ec2 describe-instances --query "Reservations[*].Instances[*].PublicIpAddress" --output=text > ntt.txt
/var/lib/jenkins/.local/bin/aws cloudformation create-stack --stack-name stNttUknpdocker-elastic --template-url https://s3.amazonaws.com/nttdatapps/ntt/docker.template --capabilities CAPABILITY_IAM --region eu-west-1
sleep 250
/var/lib/jenkins/.local/bin/aws ec2 describe-instances --query "Reservations[*].Instances[*].PublicIpAddress" --output=text > ips.tmp.txt
IP=$(awk 'NR==FNR{a[$0]=1;next}!a[$0]' ntt.txt ips.tmp.txt)
sleep 120
echo "You Manager IP address is: " $IP
echo "You cannot login to the worker, use the manager as a jump host\n"
ssh -tt -o StrictHostKeyChecking=no -i gitserver.pem docker@$IP <<'ENDSSH'
echo "getting the manager ready"
sleep 120
echo "
alias git='docker run -it --rm --name git -v $PWD:/git -w /git indiehosters/git git'
git version
git clone https://gitlab.com/aoudjet/dashboard.git
sudo chown -R $USER dashboard
cd dashboard
#./access.sh
#docker pull 267332921783.dkr.ecr.eu-west-1.amazonaws.com/elasticsearch:latest
#docker pull 267332921783.dkr.ecr.eu-west-1.amazonaws.com/kibana:latest
#docker stack deploy --with-registry-auth -c docker-stack.yml elastic
docker stack deploy -c docker-stack.yml elastic
docker stack deploy -c docker-compose.visualizer.yml visualizer

" > docker
chmod 755 docker
./docker && exit
ENDSSH
dns=$(/var/lib/jenkins/.local/bin/aws cloudformation describe-stacks --stack-name stNttUknpdocker-elastic | grep stNttUknp- | sed 's/\"//g' | sed 's/,//g' | awk '{print $2}')
echo "1- Access Manager:"
echo "http://$dns:9999"
echo "1- Access Kibana:"
echo "http://$dns:5601"

